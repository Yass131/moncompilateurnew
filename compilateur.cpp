// AUTEUR : ATMANI Yasser

#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {UNSIGNED_INT, BOOLEAN};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

map<string, enum TYPES> DeclaredVariables;

void Statement(void);
void StatementPart(void);

unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}

void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
enum TYPES Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return UNSIGNED_INT;
}

enum TYPES Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return UNSIGNED_INT;
}

enum TYPES Expression(void);

enum TYPES Factor(void){
	enum TYPES type;
	switch(current){
		case RPARENT:
			current=(TOKEN) lexer->yylex();
			type=Expression();
			if(current!=LPARENT)
				Error("')' était attendu");		// ")" expected
			else
				current=(TOKEN) lexer->yylex();
			break;
		case NUMBER:
			type=Number();
			break;
		case ID:
			type=Identifier();
			break;
		default:
			Error("'(', ou constante ou variable attendue.");
	};
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
	{
		opmul=MUL;
	}
	else if(strcmp(lexer->YYText(),"/")==0)
	{
		opmul=DIV;
	}
	else if(strcmp(lexer->YYText(),"%")==0)
	{	
		opmul=MOD;
	}
	else if(strcmp(lexer->YYText(),"&&")==0)
	{	
		opmul=AND;	
	}
	else 
	{
		opmul=WTFM;
	}
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(void){
	enum TYPES typeFact1;
	enum TYPES typeFact2;
	OPMUL mulop;
	typeFact1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		typeFact2=Factor();
		if(typeFact2!=typeFact1)
		{
			Error("ERREUR : types incompatibles dans l'expression");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				if(typeFact2!=BOOLEAN)
				{
					Error("ERREUR : le type de l'expression doit être BOOLEAN");
				}
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(typeFact2!=UNSIGNED_INT)
				{
					Error("ERREUR : le type de l'expression doit être UNSIGNED_INT");
				}
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				if(typeFact2!=UNSIGNED_INT)
				{
					Error("ERREUR : le type de l'expression doit être UNSIGNED_INT");
				}
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				if(typeFact2!=UNSIGNED_INT)
				{
					Error("ERREUR : le type de l'expression doit être UNSIGNED_INT");
				}
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return typeFact1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
	{
		opadd=ADD;
	}
	else if(strcmp(lexer->YYText(),"-")==0)
	{
		opadd=SUB;
	}
	else if(strcmp(lexer->YYText(),"||")==0)
	{
		opadd=OR;
	}
	else 
	{
		opadd=WTFA;
	}
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPES SimpleExpression(void){
	OPADD adop;
	enum TYPES typeSimple1;
	enum TYPES typeSimple2;
	typeSimple1=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		typeSimple2=Term();
		if(typeSimple1!=typeSimple2)
		{
			Error("ERREUR simpleExpr : types incompatibles dans l'expression TEST");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				if(typeSimple2!=BOOLEAN)
				{
					Error("ERREUR : le type de l'expression doit être BOOLEAN");
				}
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				if(typeSimple2!=UNSIGNED_INT)
				{
					Error("ERREUR : le type de l'expression doit être UNSIGNED_INT");
				}
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:
				if(typeSimple2!=UNSIGNED_INT)
				{
					Error("ERREUR : le type de l'expression doit être UNSIGNED_INT");
				}
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return typeSimple1;
}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if(current!=RBRACKET)
	{
		Error("caractère '[' attendu");
	}
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
	{
		Error("Un identificateur était attendu");
	}
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables[lexer->YYText()]=UNSIGNED_INT;
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
		{
			Error("Un identificateur était attendu");
		}
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables[lexer->YYText()]=UNSIGNED_INT;
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
	{
		Error("caractère ']' attendu");
	}
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
	{
		oprel=EQU;
	}
	else if(strcmp(lexer->YYText(),"!=")==0)
	{
		oprel=DIFF;
	}
	else if(strcmp(lexer->YYText(),"<")==0)
	{
		oprel=INF;
	}
	else if(strcmp(lexer->YYText(),">")==0)
	{
		oprel=SUP;
	}
	else if(strcmp(lexer->YYText(),"<=")==0)
	{
		oprel=INFE;
	}
	else if(strcmp(lexer->YYText(),">=")==0)
	{
		oprel=SUPE;
	}
	else 
	{
		oprel=WTFR;
	}
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(void){
	OPREL oprel;
	enum TYPES typeExpr1;
	enum TYPES typeExpr2;
	typeExpr1=SimpleExpression();
	if(current==RELOP)
	{
		oprel=RelationalOperator();
		typeExpr2=SimpleExpression();
		if(typeExpr1!=typeExpr2)
		{
			Error("ERREUR : types incompatibles dans l'expression");
		}
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOLEAN;
	}
	return typeExpr1;
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	enum TYPES typeAssign1;
	enum TYPES typeAssign2;
	string variable;
	if(current!=ID)
	{
		Error("Identificateur attendu");
	}
	if(!IsDeclared(lexer->YYText()))
	{
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	typeAssign1=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
	{
		Error("caractères ':=' attendus");
	}
	current=(TOKEN) lexer->yylex();
	typeAssign2=Expression();
	if(typeAssign1!=typeAssign2)
	{
		cerr << typeAssign1 << " et " << typeAssign2 << endl;
		Error("Types incompatibles pour l'affectation :");
	}
	cout << "\tpop "<<variable<<endl;
}

// DisplayStatement := "DISPLAY" Expression
void DisplayStatement(void){
	enum TYPES type;
	unsigned long long tag=++TagNumber;
	current=(TOKEN) lexer->yylex();
	type=Expression();
	switch(type){
	case UNSIGNED_INT:
		cout << "\tpop %rsi\t# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
		break;
	case BOOLEAN:
			cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
			cout << "\tcmpq $0, %rdx"<<endl;
			cout << "\tje False"<<tag<<endl;
			cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
			cout << "\tjmp Next"<<tag<<endl;
			cout << "False"<<tag<<":"<<endl;
			cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
			cout << "Next"<<tag<<":"<<endl;
			cout << "\tcall	puts@PLT"<<endl;
			break;
	default:
			Error("DISPLAY ne fonctionne pas pour ce type de donnée.");
		}

}

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	int tag=TagNumber++;
	enum TYPES typeIf1;
	if(current==KEYWORD && strcmp(lexer->YYText(), "IF")==0)
	{
		cout << "IF" << tag << ":" << endl;
		current=(TOKEN) lexer->yylex();
		typeIf1=Expression();
		if(typeIf1!=BOOLEAN)
		{
			Error("ERREUR : Expression de type BOOLEAN attendue");
		}
		
		cout<<"\tpop %rax"<<endl;
		cout<<"\tcmpq $0, %rax"<<endl;
		cout<<"\tje ELSE"<<tag<<endl;
		
		cout << "THEN" << tag << ":" << endl;
		if(current==KEYWORD && strcmp(lexer->YYText(), "THEN")==0)
		{
			current=(TOKEN) lexer->yylex();
			Statement();
			
			cout<<"\tjmp FINSI"<<tag<<endl;
			
			cout << "ELSE" << tag << ":" << endl;
			if(current==KEYWORD && strcmp(lexer->YYText(), "ELSE")==0)
			{
				current=(TOKEN) lexer->yylex();
				Statement();
			}
		}
		else
		{
			Error ("Mot-cle 'THEN' attendu");
		}
		cout << "FINSI" << tag << ":" << endl;
	}
	else
	{
			Error ("Mot-cle 'IF' attendu");
	}
}

// WhileStatement := "WHILE" Expression DO Statement
void WhileStatement(void){
	int tag= ++TagNumber;
	enum TYPES typeWhile1;
	if(current==KEYWORD && strcmp(lexer->YYText(), "WHILE")==0)
	{
		cout << "WHILE" << tag << ":" << endl;
		current=(TOKEN) lexer->yylex();
		typeWhile1=Expression();
		if(typeWhile1!=BOOLEAN)
		{
			Error("ERREUR : Expression de type BOOLEAN attendue");
		}
		
		cout<<"\tpop %rax"<<endl;
		cout<<"\tcmpq $0, %rax"<<endl;
		cout<<"\tje FINWHILE"<<tag<<endl;
		
		cout << "DO" << tag << ":" << endl;
		if(current==KEYWORD && strcmp(lexer->YYText(), "DO")==0)
		{
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		cout<<"\tjmp WHILE"<<tag<<endl;
		cout<<"FINWHILE"<<tag<<":"<<endl;
	}
	else
	{
		Error("Mot-cle 'WHILE' attendu");
	}
}

// ForStatement := "For" AssignementStatement ("TO"|"DOWNTO") Expression "DO" Statement
void ForStatement(void){
	int tag= ++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(), "FOR")==0)
	{
		cout << "FOR" << tag << ":" << endl;
		current=(TOKEN) lexer->yylex();
		string variable = lexer->YYText(); // ON STOCKE LA VARIABLE ENTREE AVANT DE PASSER A AssignementStatement POUR POURVOIR L'INCREMENTER
		AssignementStatement();
		if(current==KEYWORD && strcmp(lexer->YYText(), "TO")==0)
		{
			cout << "TO" << tag << ":" << endl;
			current=(TOKEN) lexer->yylex();
			Expression();
			cout<<"\tpop %rax"<<endl;
			cout<<"\tpush "<<variable<<endl;
			cout<<"\tpop %rbx"<<endl;
			cout<<"\tcmpq %rax, %rbx"<<endl;
			cout<<"\tja FINFOR"<<tag<<endl;
			if(current==KEYWORD && strcmp(lexer->YYText(), "DO")==0)
			{
				cout << "DO" << tag << ":" << endl;
				current=(TOKEN) lexer->yylex();
				Statement();
				
				cout<<"\taddq $1,"<<variable<<endl;
				cout<<"\tjmp TO"<<tag<<endl;
				cout<<"\tFINFOR"<<tag<<":"<<endl;
			}
			else
			{
				Error("Mot-cle 'DO' attendu");
			}
		}
		else if(strcmp(lexer->YYText(), "DOWNTO")==0)
		{
			cout << "DOWNTO" << tag << ":" << endl;
			current=(TOKEN) lexer->yylex();
			Expression();
			cout<<"\tpop %rax"<<endl;
			cout<<"\tpush "<<variable<<endl;
			cout<<"\tpop %rbx"<<endl;
			cout<<"\tcmpq %rax, %rbx"<<endl;
			cout<<"\tjb FINFOR"<<tag<<endl;
			if(current==KEYWORD && strcmp(lexer->YYText(), "DO")==0)
			{
				cout << "DO" << tag << ":" << endl;
				current=(TOKEN) lexer->yylex();
				Statement();
				
				cout<<"\tsubq $1,"<<variable<<endl;
				cout<<"\tjmp DOWNTO"<<tag<<endl;
				cout<<"\tFINFOR"<<tag<<":"<<endl;
			}
			else
			{
				Error("Mot-cle 'DO' attendu");
			}
		}
		else
		{
			Error("Mot-cle 'TO' ou 'DOWNTO' attendu");
		}
	}
	else
	{
		Error("Mot-cle 'FOR' attendu");
	}
}

void BlockStatement(void){
	current=(TOKEN) lexer->yylex(); //lexer->yylex(); on passe au TOKEN suivant
	Statement();
	while(current==SEMICOLON)
	{
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(strcmp(lexer->YYText(),"END")!=0) //lexer->YYText(); pour lire la chaîne elle-même
	{
		Error("mot-clé 'END' attendu");
	}
	current=(TOKEN) lexer->yylex();
}

// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayAssignement
void Statement(void){
	if(current==KEYWORD)
		{
			if (strcmp(lexer->YYText(),"IF")==0)
			{
				IfStatement();
			}
			else if (strcmp(lexer->YYText(),"WHILE")==0)
			{
				WhileStatement();
			}
			else if (strcmp(lexer->YYText(),"BEGIN")==0)
			{
				BlockStatement();
			}
			else if (strcmp(lexer->YYText(),"FOR")==0)
			{
				ForStatement();
			}
			else if (strcmp(lexer->YYText(),"DISPLAY")==0)
			{
				DisplayStatement();
			}
		}
		
	else
	{
		if(current==ID)
			{
				AssignementStatement();
			}
		
		else
		{
			Error ("Instruction attendue");
		}
	}
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON)
	{
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
	{
		Error("caractère '.' attendu");
	}
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\"\t# used by printf to display 64-bit unsigned integers"<<endl;
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





