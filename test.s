			# This code was produced by the CERI Compiler
.data
FormatString1:	.string "%llu\n"	# used by printf to display 64-bit unsigned integers
	.data
	.align 8
i:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
FOR1:
	push $10
	pop i
DOWNTO1:
	push $5
	pop %rax
	push i
	pop %rbx
	cmpq %rax, %rbx
	jb FINFOR1
DO1:
	push i
	pop %rsi	# The value to be displayed
	movq $FormatString1, %rdi	# "%llu\n"
	movl	$0, %eax
	call	printf@PLT
	subq $1,i
	jmp DOWNTO1
	FINFOR1:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
