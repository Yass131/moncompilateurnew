# CERIcompiler

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

**This version Can handle :**

> // Program := [DeclarationPart] StatementPart
> // DeclarationPart := "[" Identifier {"," Identifier} "]"
> // StatementPart := Statement {";" Statement} "."
> // Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayStatement
> // AssignementStatement := Identifier ":=" Expression
> // DisplayStatement := "DISPLAY" Expression
> // IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
> // WhileStatement := "WHILE" Expression DO Statement
> // ForStatement := "For" AssignementStatement ("TO"|"DOWNTO") Expression "DO" Statement

> // Expression := SimpleExpression [RelationalOperator SimpleExpression]
> // SimpleExpression := Term {AdditiveOperator Term}
> // Term := Factor {MultiplicativeOperator Factor}
> // Factor := Number | Letter | "(" Expression ")"| "!" Factor
> // Number := Digit{Digit}
> // Identifier := Letter {(Letter|Digit)}

> // AdditiveOperator := "+" | "-" | "||"
> // MultiplicativeOperator := "*" | "/" | "%" | "&&"
> // RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
> // Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
> // Letter := "a"|...|"z"
> // Type := "UNSIGNED_INT" | "BOOLEAN"

**L'utilisation du compilateur**

Le compilateur différencie 2 types de variables : UNSIGNED_INT (entiers), et BOOLEAN (variables booléennes). 

Il y a également une vérification sur la compatibilité des types qui se fait, on ne peut donc pas écrire "IF 3+TRUE DO".

La boucle FOR accepte les mot-clés TO mais aussi DOWNTO pour effectuer une boucle descendante. L'instruction "FOR i:=4 DOWNTO 1 DO" est donc acceptée.

Enfin, il y a l'implémentation de la fonction Display avec le mot-clé DISPLAY qui affiche des éléments sur le terminal.

> make test

La commande ci-dessus pour compiler le fichier test.p grâce à un makefile, ce qui produira un fichier de code assembleur test.s.

> ./test

La commande ci-dessus pour exécuter le fichier test et voir les élements affichés par DISPLAY.
 
**Fichier test.p**

[i]
FOR i := 10 DOWNTO 5 DO
BEGIN
	DISPLAY i;
END
.

